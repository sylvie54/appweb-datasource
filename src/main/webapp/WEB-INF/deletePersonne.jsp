<%-- 
    Document   : saisiePersonne
    Created on : 4 avr. 2020, 22:45:00
    Author     : Sylvie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="AFPA.CDA03.demo.appWeb.models.Personne"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Suppression personne</title>
    </head>
    <body>
        <h3>Supression personne</h3>
        <p> nombre de pages lues :  ${ compteurPage }</p>
        <p> ${ message }</p>
           <form name="deletePersonne" method="post">
            <c:if test="${!empty personnes}" > 
                
            <select id="personne" name = "personne"  >
                <c:forEach var="personne" begin="0" items="${personnes}">
                    <option value="${ personne.idpersonne }">  ${ personne.nom } ${personne.prenom}  </option> 
                </c:forEach>
            </select> 
            </c:if>  
            <c:if test="${empty personnes}" >
                <p>personne à supprimer :  </p>
               <input type="hidden" name="idpersonne" id="idpersonne" value="${ idpersonne}" >
               Nom : <input type="text" name="nom" id="nom" value="${ ancnom }" size="50" readonly /><br><br>
               Prénom  : <input type="text" name="prenom" id="prenom" value="${ ancprenom }" size="50" readonly /><br>
            </c:if>    
         
            <input type="submit" value="Envoi" name="BtnSubmit" /> </p>
    </form>
    <P> <a href="?cmd=page4">liste personne</a> </p>       
    </body>
</html>
