<%-- 
    Document   : erreur
    Created on : 7 avr. 2020, 10:57:28
    Author     : Sylvie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>page d'erreur</title>
    </head>
    <body>
        <h1>Erreur</h1>
        <img src="<%=request.getContextPath()%>/images/erreur.jpg" wight="200" height="170" alt=""/>
        <h2> ${ requestScope.message }</h2>
    </body>
</html>
