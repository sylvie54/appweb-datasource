<%-- 
    Document   : essai
    Created on : 3 avr. 2020, 21:13:39
    Author     : Sylvie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="AFPA.CDA03.demo.appWeb.models.Personne"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Liste de personnes</title>
    </head>
    <body>
        <h3>Liste adhérents</h3>
          <p> nombre de pages lues :  ${ compteurPage }</p>
        
        <c:if test="${!empty personnes}" >
            <p>nombre de personnes : ${ nombre }</p>
          <table border="1">
              <tr>
                  <td>Identifiant</td>
                  <td>Nom</td>
                  <td>Prénom</td>
              </tr>
            <c:forEach var="personne" begin="0" items="${personnes}">
                <tr>
                    <td>${personne.idpersonne}</td>
                    <td>${ personne.nom }</td> 
                    <td>${ personne.prenom }</td> 
                </tr>
            </c:forEach> 
          </table>        
        </c:if>       
                <P> <a href="?cmd=page1">saisie personne</a> </p>         
                <P> <a href="?cmd=page2">mise à jour personne</a> </p>         
                <P> <a href="?cmd=page3">suppression personne</a> </p>         
                      
                  
    </body>
</html>
