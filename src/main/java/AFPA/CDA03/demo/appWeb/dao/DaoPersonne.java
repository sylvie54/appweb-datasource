/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFPA.CDA03.demo.appWeb.dao;

import AFPA.CDA03.demo.appWeb.controllers.ListePersonneController;
import AFPA.CDA03.demo.appWeb.models.Personne;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.sql.DataSource;
import AFPA.CDA03.demo.appWeb.servlet.FrontController;


/**
 *
 * @author Sylvie
 */
public class DaoPersonne {
    private static final Logger LOGGER = 
    Logger.getLogger(ListePersonneController.class.getName());
    private  static final Connection CONNEXION = FrontController.connection;
    private static ResultSet resultat = null;
//        try {
//            Context initCtx = new InitialContext();
//            Context envCtx = (Context) initCtx.lookup("java:comp/env");
//    
//            // Look up our data source
//            datasource = (DataSource) envCtx.lookup("jdbc/bddappweb");
//        }
//        catch (Exception e) {
//            new ServletException("Couldn't find the Datasource", e);
//        }
    
    public static Personne resultSetPersonne(ResultSet results) throws SQLException {
        Personne personne = new Personne();
        personne = new Personne();
                personne.setIdpersonne(results.getInt(1));
                personne.setNom( results.getString(2));
                personne.setPrenom(results.getString(3));
        return personne;
    }
    
    public static Personne findPersonById(int id) {
       Personne p = null;
        try {
            PreparedStatement stm = CONNEXION.prepareStatement("SELECT *  FROM personne where idpersonne =  ?"); 

            stm.setInt(1, id);
            resultat = stm.executeQuery();
        
            while (resultat.next())
            {
               p = resultSetPersonne(resultat);
            }
        }    
        catch (SQLException sqle) {
            LOGGER.severe(" problème lecture table personne " + sqle.getMessage());
        }
       return p;
    }        
    public static List<Personne> findAll() throws SQLException {
        List personnes = new ArrayList<Personne>();
        try {
            PreparedStatement statement = CONNEXION.prepareStatement("select * from personne");
            ResultSet results = statement.executeQuery();
            Personne personne;
            while (results.next()) {
                personne = resultSetPersonne(results);
                personnes.add(personne);
            }
       //     LOGGER.warning("----  personnes) "+personnes.toString());
        }
        catch (Exception e  ) {
            LOGGER.severe(" problème lecture table personne " + e.getMessage());
        }
 //       em.close();
        return personnes;
    }
    public static void save(Personne personne) {
//     Todo
    }
    public static void delete(int id) {
//  Todo
    }
}
