/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFPA.CDA03.demo.appWeb.controllers;

import AFPA.CDA03.demo.appWeb.dao.DaoPersonne;
import AFPA.CDA03.demo.appWeb.models.Personne;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Sylvie
 */
public class ListePersonneController implements ICommand {
    private static final Logger LOGGER = 
    Logger.getLogger(ListePersonneController.class.getName());
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception
    { 
    String urlSuite = "listePersonnes.jsp";    
    try {
        HttpSession session = request.getSession();
        int compteurPage;
        Integer sessionCompteurPage = (Integer) ( session.getAttribute("compteurPage"));
        if (sessionCompteurPage == null ) {
            compteurPage = 0;
        }
        else {
            compteurPage = sessionCompteurPage;
        }
        compteurPage = compteurPage + 1; 
        session.setAttribute("compteurPage", compteurPage);
        request.setAttribute("compteurPage",compteurPage); 
        List listPersonnes = DaoPersonne.findAll();
        request.setAttribute("personnes",listPersonnes); 
        request.setAttribute("nombre",listPersonnes.size()); 
    }
    catch (Exception e) {
        LOGGER.severe("----------  problème listePersonneController " + e.getMessage());
        request.setAttribute("message", "un problème est survenu, site inaccessible"); 
        urlSuite = "erreur.jsp";
    }
    
    return (urlSuite);
}
}    
