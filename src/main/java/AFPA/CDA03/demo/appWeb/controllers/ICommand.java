/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFPA.CDA03.demo.appWeb.controllers;

/**
 *
 * @author Utilisateur
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Acer
 */
public interface ICommand 
{
   String execute(HttpServletRequest request, HttpServletResponse response) throws Exception;
 } 
