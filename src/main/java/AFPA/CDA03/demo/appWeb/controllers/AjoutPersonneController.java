/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFPA.CDA03.demo.appWeb.controllers;

import AFPA.CDA03.demo.appWeb.dao.DaoPersonne;
import AFPA.CDA03.demo.appWeb.models.Personne;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Sylvie
 */
public class AjoutPersonneController implements ICommand {
    private static final Logger LOGGER =   Logger.getLogger(ListePersonneController.class.getName());
    public String execute(HttpServletRequest request, HttpServletResponse response)  throws Exception 
    { 
        String urlSuite = "saisiePersonne.jsp";
        try {
            HttpSession session = request.getSession();
            Integer sessionCompteurPage = (Integer) ( session.getAttribute("compteurPage"));
            sessionCompteurPage++;
            session.setAttribute("compteurPage",sessionCompteurPage);
            request.setAttribute("compteurPage",sessionCompteurPage); 
            String message = "";
            if (request.getParameterMap().containsKey("nom")) {    
                String nom  = (String) request.getParameter("nom");
                String prenom  = (String) request.getParameter("prenom");
                if (nom.isEmpty() || prenom.isEmpty()) {
                    message = "nom ou prenom vides";
                    request.setAttribute("ancnom", nom);
                    request.setAttribute("ancprenom", prenom);
                    request.setAttribute("message", message);
                }
                else
                {    
                    Personne personne = new Personne(nom,prenom);
                    DaoPersonne.save(personne);
                    List listPersonnes = DaoPersonne.findAll();
                    request.setAttribute("personnes",listPersonnes); 
                    request.setAttribute("nombre",listPersonnes.size()); 
                    urlSuite ="listePersonnes.jsp";
                }    
            }
        }    
        catch (Exception e) {
            LOGGER.severe("----------  pb AjoutPersonneController " + e.getMessage());
            request.setAttribute("message", "un problème est survenu, le site est inaccessible"); 
            urlSuite = "erreur.jsp";
        }
        return (urlSuite);
}
    
}
