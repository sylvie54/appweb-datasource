/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFPA.CDA03.demo.appWeb.controllers;

import AFPA.CDA03.demo.appWeb.dao.DaoPersonne;
import AFPA.CDA03.demo.appWeb.models.Personne;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Sylvie
 */
public class DeletePersonneController implements ICommand {
    private static final Logger LOGGER =   Logger.getLogger(ListePersonneController.class.getName());
    public String execute(HttpServletRequest request, HttpServletResponse response)  throws Exception
    { 
        List listPersonnes = null;
        Personne updatedPers = null;
        HttpSession session = request.getSession();
        Integer sessionCompteurPage = (Integer) ( session.getAttribute("compteurPage"));
        sessionCompteurPage++;
        session.setAttribute("compteurPage",sessionCompteurPage);
        request.setAttribute("compteurPage",sessionCompteurPage); 
        String urlSuite = "deletePersonne.jsp";        
        try {
            if (!request.getParameterMap().containsKey("personne") && !request.getParameterMap().containsKey("nom")) {  
                listPersonnes = DaoPersonne.findAll();
                request.setAttribute("personnes",listPersonnes); 
            }
            if (request.getParameterMap().containsKey("personne")) {
                String selectPersonne  = (String) request.getParameter("personne");
                updatedPers = DaoPersonne.findPersonById(Integer.parseInt(selectPersonne));
                request.setAttribute("idpersonne", updatedPers.getIdpersonne());
                request.setAttribute("ancnom", updatedPers.getNom());
                request.setAttribute("ancprenom", updatedPers.getPrenom());

            }
            if (request.getParameterMap().containsKey("nom")) { 
                String updateId = (String) request.getParameter("idpersonne");
                DaoPersonne.delete(Integer.parseInt(updateId));
                listPersonnes = DaoPersonne.findAll();
                request.setAttribute("personnes",listPersonnes); 
                request.setAttribute("nombre",listPersonnes.size()); 
                urlSuite ="listePersonnes.jsp";
            }
        }    
        catch (Exception e) {
            LOGGER.severe("----------  pb DeletePersonneController " + e.getMessage());
            request.setAttribute("message", "un problème est survenu, le site est inaccessible"); 
            urlSuite = "erreur.jsp";
        }
        return (urlSuite);
}   
}
