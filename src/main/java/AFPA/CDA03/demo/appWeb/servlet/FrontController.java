/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFPA.CDA03.demo.appWeb.servlet;

import AFPA.CDA03.demo.appWeb.controllers.AjoutPersonneController;
import AFPA.CDA03.demo.appWeb.controllers.DeletePersonneController;
import AFPA.CDA03.demo.appWeb.controllers.ICommand;
import AFPA.CDA03.demo.appWeb.controllers.ListePersonneController;
import AFPA.CDA03.demo.appWeb.controllers.UpdatePersonneController;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 *
 * @author Sylvie
 */
@WebServlet(
        name = "MyServlet",
        urlPatterns = {"/hello"}
    )
public class FrontController extends HttpServlet {
    private Map commands=new HashMap();
    private static final Logger LOGGER =   Logger.getLogger(ListePersonneController.class.getName());
    int compteurPage;
    public static Connection connection;
    @Resource(name="jdbc/bddappweb")public static DataSource datasource;

    public void init()
    { 
        commands.put(null, new ListePersonneController()) ;  
        commands.put("page1", new AjoutPersonneController()) ;
        commands.put("page2", new UpdatePersonneController()) ;
        commands.put("page3", new DeletePersonneController()) ;
        commands.put("page4", new ListePersonneController()) ;
        
        try {
            connection = datasource.getConnection(); 
            LOGGER.warning("------ connection open");
        }
        catch (SQLException e) {
            new ServletException("Couldn't open the Connection");
            LOGGER.severe("------ Couldn't open the Connection" + e);
        }
    }
    public void destroy() {
        try {
            connection.close();
            LOGGER.warning("------ connection close");
        }
        catch (SQLException e) {
            new ServletException("Couldn't close the Connection");
            LOGGER.severe("----- Couldn't close the Connection" + e);
        }
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        // Ouverture des sessions
         HttpSession session = request.getSession(true);
                
        String urlSuite= "";
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        // On récupère le paramètre passé par la page précédente ex : "ServletMvc?cmd=page1"
        String cmd=request.getParameter("cmd");
        // On récupère la classe "controleur" qui se trouve en valeur de la clé passée en paramètre et on instencie un objet com de cette classe
        // ex : CtrlPageClasse() pour paramètre égal à 1
        try
        {    
        ICommand com=(ICommand)commands.get(cmd);
        // On appelle la méthode "execute" de cette classe et on récupère  dans "urlsuite" l'url de la page à afficher 
        // plus toutes les info nécessaire à la page à afficher dans "resquest"
        urlSuite=com.execute(request, response);

        }
        catch (Exception e)
                {
                   LOGGER.severe("pb front controller " + cmd + " "  + e.getMessage() );
                   request.setAttribute("message", "un problème est survenu, le site est inaccessible"); 
                   urlSuite = "erreur.jsp";
                }
        finally

        {
            request.getRequestDispatcher("WEB-INF/"  +urlSuite).forward(request, response); 
        }

     
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
        
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    }
