/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFPA.CDA03.demo.appWeb.models;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Sylvie
 */

public class Personne implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer idPersonne;
    
    @NotBlank(message = "le nom ne doit pas être vide")
    @Size(max=30, message = " le nom doit faire moins de 30 caratères ") 
    private String nom;
     
    @NotNull(message = " le prénom ne doit pas être vide ")
    @Size(min=2, message=" le prénom doit faire au moins 2 caractères ")
    @Size(max=30, message = " le prénom doit faire moins de 30 caratères ")
    private String prenom;    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (int) (this.idPersonne ^ (this.idPersonne >>> 32));
        hash = 29 * hash + Objects.hashCode(this.nom);
        hash = 29 * hash + Objects.hashCode(this.prenom);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personne other = (Personne) obj;
        if (this.idPersonne != other.idPersonne) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        return true;
    }
    
    public Personne() {
    }

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public Personne(Integer idpersonne, String nom, String prenom) {
        this.idPersonne = idpersonne;
        this.nom = nom;
        this.prenom = prenom;
    }

    
    
    @Override
    public String toString() {
        return "Client{" + "idpersonne=" + idPersonne + ", nom=" + nom + ", prenom=" + prenom + '}';
    }

   

    public Integer getIdpersonne() {
        return idPersonne;
    }

    public void setIdpersonne(Integer idpersonne) {
        this.idPersonne = idpersonne;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

   
}
